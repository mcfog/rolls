package mcfog.rolls.battlerounds;

import mcfog.rolls.battlerounds.exceptions.BattleRoundsException;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ExceptionsTest {

    @Test
    public void BattleRoundsExceptionTest() {

        try {
            throw new BattleRoundsException("Meow");
        } catch (BattleRoundsException e){
            assertThat(e.getMessage(), is("BattleRounds' System Error: Meow"));
        }

    }

}
