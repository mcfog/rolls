package mcfog.rolls.commands;

import joptsimple.internal.Strings;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class TestCommand implements ICommand {
    private final List<String> aliases;
    private String CmdName;
    private String CmdUsage;
    private ArrayList<Integer> results;

    public TestCommand(){

        System.out.println("TestBtlCommand Registering");

        CmdName = "TestBtlCommand";
        CmdUsage = "test <any>";
        aliases = new ArrayList<>();
        aliases.add("test");
        aliases.add("t");
    }

    @Override
    public String getName() {
        return CmdName;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return CmdUsage;
    }

    @Override
    public List<String> getAliases() {
        return this.aliases;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        System.out.println("command:" + CmdName);
        System.out.println("args:" + Strings.join(args, ","));

        String text = "test meow";
        sender.sendMessage(new TextComponentString(text));
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
