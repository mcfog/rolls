package mcfog.rolls.commands;

import mcfog.rolls.battlerounds.commands.BtlCommand;
import net.minecraft.command.ICommand;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.util.ArrayList;

public enum CommandRegister {

    INSTANCE;

    private final ArrayList<ICommand> commands;

    private void defaultCommands() {
        addCommand(new BtlCommand());
    }

    private CommandRegister(){
        this.commands = new ArrayList<>();
    }

    public void addCommand(ICommand command){
        this.commands.add(command);
    }

    public void Register(FMLServerStartingEvent event) {
        if (this.commands.size() != 0){
            for (ICommand command : this.commands) {
                event.registerServerCommand(command);
            }
        }
    }
}
