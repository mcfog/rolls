package mcfog.rolls.battlerounds.exceptions;

// Throw when manager not exists in binded battles list
public class ManagerNotBindedException extends Throwable {
}
