package mcfog.rolls.battlerounds.exceptions;

public class BattleRoundsException extends Throwable {

    public static final String summury = "BattleRounds' System Error";

    public BattleRoundsException(){
        super(summury);
    }

    public BattleRoundsException(String message){
        super(summury + ": " + message);
    }
}
