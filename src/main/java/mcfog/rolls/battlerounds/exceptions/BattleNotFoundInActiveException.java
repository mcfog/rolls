package mcfog.rolls.battlerounds.exceptions;

// Throw when Battle not exists in active battles list
public class BattleNotFoundInActiveException extends Throwable{

}
