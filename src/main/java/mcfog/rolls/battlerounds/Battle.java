package mcfog.rolls.battlerounds;

import mcfog.rolls.util.UUIDGenerator;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class Battle {

    // Unique Battle ID
    private final String ID;

    // Main User List
    private final CopyOnWriteArrayList<String> userlist;

    // Rounds Line History
    private final CopyOnWriteArrayList<Round> rounds;

    private Round CURRENT_ROUND;

    Battle(String ID) {
        this.ID = ID;

        userlist = new CopyOnWriteArrayList<>();
        rounds = new CopyOnWriteArrayList<>();

    }

    // `/btl create`
    public static final Battle create() {
        String ID = UUIDGenerator.getUUID();
        Battle btl = new Battle(ID);

        // add first Round
        btl.nextRound();

        //BattleManager.INSTANCE.addBattle(btl);

        return  btl;
    }

    // `/btl stop`
    public void stop() {
        //BattleManager.INSTANCE.deleteBattle(this);
    }

    // Get Battle ID
    public String ID(){
        return this.ID;
    }

    // End Turn for Current User in Current Round
    public void endTurn() {
        if (!getCurrentRound().isOrderEmpty()) {
            getCurrentRound().endTurn();
        }
    }

    public boolean isCurrentOrderEmpty() {
        return getCurrentRound().isOrderEmpty();
    }

    // Get Current Entry in Round Order List
    public Map.Entry<String, Double> getCurrentEntryOrder() {
        return getCurrentRound().getCurrentUserOrder();
    }

    // Get Current Username in Round Order List
    public String getCurrentOrderUsername(){
        if(getCurrentEntryOrder() == null){
            return null;
        }
        return getCurrentEntryOrder().getKey();
    }

    // Get Current Reaction in Round Order List
    public Double getCurrentOrderUserReaction(){

        if(getCurrentEntryOrder() == null){
            return null;
        }

        return getCurrentEntryOrder().getValue();
    }

    public Map<String, Double> getCurrentOrderList() {
        return getCurrentRound().getOrderMap();
    }

    // Return the current Round
    public Round getCurrentRound() {
//        if (CURRENT_ROUND == null) {
//            if (rounds.size() == 0){
//                nextRound();
//            } else {
//                setLastRoundAsCurrent();
//            }
//        }

        return CURRENT_ROUND;
    }

    // Set the last Round from rounds list as Current Round
    protected void setLastRoundAsCurrent() {
        setCurrentRound(rounds.size() - 1);
    }

    // Set Current Round by index
    protected void setCurrentRound(int index) {
        if (index >= 0 && index <= rounds.size() - 1) {
            CURRENT_ROUND = rounds.get(index);
        }
    }

    // Set Current Round by object
    protected void setCurrentRound(Round round) {
        if (rounds.contains(round)) CURRENT_ROUND = round;
    }

    // Set Current Round as round param and add round to rounds List
    protected void setHardCurrentRound(Round round) {
        CURRENT_ROUND = round;
        if (!rounds.contains(round)) {
            addNewRound(round);
        }
    }

    // Add new Round to Battle
    // return: index of last Round
    protected int addNewRound(){
        rounds.add(Round.create(this));
        return rounds.size() - 1;
    }

    // Add new Round as round param
    // return last index
    protected int addNewRound(Round round) {
        rounds.add(round);
        return rounds.size() - 1;
    }

    // Add new User to Battle UserList
    // `/btl add <username>`
    public void addUser(String user){
        if (!userlist.contains(user)) {
            userlist.add(user);
        } else {
          /* TODO:
                * return result OK or no
           */
        }
    }

    // Add User List from Battle UserList
    // `/btl add <username1> <username2> <username3> ...`
    public void addUser(String[] users){
        for (String user : users){
            addUser(user);
            /* TODO:
                * collect results with not OK and return as List with no added users
             */
        }
    }

    // Del User from Battle UserList
    // `/btl del <username>`
    public void deleteUser(String user){
        if (userlist.contains(user)) {
            getCurrentRound().deleteUser(user);
            userlist.remove(user);
        } else {
            /* TODO:
                * return result OK or no
             */
        }
    }

    // Del User List from Battle UserList
    // `/btl del <username1> <username2> <username3> ...`
    public void deleteUser(String[] users) {
        for (String user : users) {
            deleteUser(user);
            /* TODO:
                * collect results with not OK and return as List with no deleted users
            */
        }
    }

    protected List<String> getArrayUserList() {
        return Collections.unmodifiableList(userlist);
    }

    // Get Battle User List as String[]
    public String[] getUserList(){
        return (String[]) userlist.toArray(new String[0]);
    }

    // Get Battle User List as List
    public List<String> getUserListAsList(){
        return Collections.unmodifiableList(userlist);
    }

    // `/btl chreact <username> <number>`
    public void setUserReaction(double reaction, String username){

        if (userlist.contains(username)) {
            getCurrentRound().setUserReaction(reaction, username);
        }
    }

    // /btl setreact <number1> <number2> <number3> ...
    public void setReactionsByUserList(double[] reactions){

        if (userlist.size() == reactions.length) {
            getCurrentRound().setReactionsByUserList(reactions);
        }
    }

    // `/btl nextround`
    public void nextRound() {

        int lastRoundIndex = addNewRound();
        setLastRoundAsCurrent();

    }

    // `/btl makeorder`
    public void makeOrder() {

        // start **listening chat reactions**
        // set Reactions

        System.out.println("BattleRounds: emulating makeOrder()...");

        // stop **listening chat reactions** and do next step
        // stop setting Reactions

    }

    // `/btl save <battle_name>`
    public void save(String battlename) {

    }

    // `/btl load <battle_name>`
    public void load(String battlename) {

    }

    // Show Battle UserList
    // `/btl ls`
    public void showUserList(){

    }

    // `/btl lsorder`
    public void showOrderList() {

    }

    // `/btl curr`
    public void showCurrentUserOrder() {

    }



}
