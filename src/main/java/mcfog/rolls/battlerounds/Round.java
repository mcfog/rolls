package mcfog.rolls.battlerounds;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

class Round {
    // Regenerating Order List
    // Sorted List (Map) of <Username, Reaction>
    private final ConcurrentHashMap<String, Double> ordermap;
    private final Battle battle;

    Round(Battle btl) {
        this.battle = btl;
        this.ordermap = new ConcurrentHashMap<>();
    }

    // Create new Round
    // return: new Round object
    static Round create(Battle btl) {
        return new Round(btl);
    }

    private LinkedHashMap<String, Double> getSortedOrderMap(){
        LinkedHashMap<String, Double> sorted = this.ordermap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

        return sorted;
    }

    private ArrayList<String> getSortedOrderUsernameList(){
        ArrayList<String> sorted = (ArrayList<String>) this.ordermap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

        return sorted;
    }


    // End Turn for Current User
    // Remove last user in ordermap
    void endTurn() {
        if (!ordermap.isEmpty()) {
            ordermap.remove(getSortedOrderUsernameList().get(0));
        }
    }

    boolean isOrderEmpty() {
        if (ordermap.isEmpty()) return true;
        if (ordermap.size() == 0) return true;
        return false;
    }

    Map<String, Double> getOrderMap(){
        return getSortedOrderMap();
    }

    void setUserReaction(double reaction, String user){
        ordermap.put(user, reaction);
    }

    void setReactionsByUserList(double[] reactions){

        int index = 0;
        List<String> usernames = battle.getArrayUserList();
        for (double reaction : reactions) {
            setUserReaction(reaction, usernames.get(index));
            index++;
        }
    }

    void deleteUser(String username){
        ordermap.remove(username);
    }

    Map.Entry<String, Double> getCurrentUserOrder() {

        if (ordermap.isEmpty() || ordermap.size() == 0){
            return null;
        }

        String username = getSortedOrderUsernameList().get(0);
        double reaction = ordermap.get(username);

        return new AbstractMap.SimpleEntry<String, Double>(username, reaction);
    }
}
