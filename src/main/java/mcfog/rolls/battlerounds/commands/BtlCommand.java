package mcfog.rolls.battlerounds.commands;

import mcfog.rolls.battlerounds.handlers.BtlCommandHandler;
import mcfog.rolls.util.Permissions;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class BtlCommand implements ICommand {

    private final String name;
    private final ArrayList<String> aliases;
    private final String usage;

    public static final String SUBCOMMAND_BTL_CREATE = "create";
    public static final String SUBCOMMAND_BTL_JOIN = "join";
    public static final String SUBCOMMAND_BTL_STOP = "stop";
    public static final String SUBCOMMAND_BTL_ADD = "add";
    public static final String SUBCOMMAND_BTL_LS = "ls";
    public static final String SUBCOMMAND_BTL_CHREACT = "chreact";
    public static final String SUBCOMMAND_BTL_SETREACT = "setreact";
    public static final String SUBCOMMAND_BTL_MAKEORDER = "makeorder";
    public static final String SUBCOMMAND_BTL_NEXTROUND = "nextround";
    public static final String SUBCOMMAND_BTL_ENDTURN = "endturn";
    public static final String SUBCOMMAND_BTL_LSORDER = "lsorder";
    public static final String SUBCOMMAND_BTL_CURR = "curr";
    public static final String SUBCOMMAND_BTL_DEL = "del";
    public static final String SUBCOMMAND_BTL_SAVE = "save";
    public static final String SUBCOMMAND_BTL_LOAD = "load";
    public static final String SUBCOMMAND_BTL_LSBL = "lsbl";
    public static final String SUBCOMMAND_BTL_LSB = "lsb";
    public static final String SUBCOMMAND_BTL_DELBTL = "delbtl";

    public BtlCommand(){
        name = "btl";

        aliases = new ArrayList<>();
        aliases.add("battle");
        aliases.add("btl");

        usage = "/btl <command> <arguments>";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return usage;
    }

    @Override
    public List<String> getAliases() {
        return this.aliases;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        BtlCommandHandler.handle(server, sender, args);
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {

        boolean isCan;

        if (Permissions.isSinglePlayer()) {
            return true;
        }

        isCan = Permissions.isOp(sender.getName());

        return isCan;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
