package mcfog.rolls.battlerounds;

import mcfog.rolls.battlerounds.exceptions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

public enum BattleManager {

    INSTANCE;

    // Battle List <Battle ID, Battle object>
    private final ConcurrentSkipListMap<String, Battle> activeBattles;

    // Battle List <Battle ID, Manager>
    private final ConcurrentHashMap<String, String> bindedBattles;

    // Constructor
    private BattleManager() {
        this.activeBattles = new ConcurrentSkipListMap<>();
        this.bindedBattles = new ConcurrentHashMap<>();
    }

    // Get Unmodifiable activeBattles
    public SortedMap<String, Battle> getActiveBattles(){
        return Collections.unmodifiableSortedMap(activeBattles);
    }

    // Get Unmodifiable bindedBattles
    public Map<String, String> getBindedBattles() {
        return Collections.unmodifiableMap(bindedBattles);
    }

    // Get Battle by Manager name
    // return null if Manager not binded to any Battle
    public Battle getBindedBattleByManager(String manager) {
        for (Map.Entry<String, String> pair : bindedBattles.entrySet()) {
            if (manager.equals(pair.getValue())) {
                return activeBattles.get(pair.getKey());
            }
        }
        return null;
    }

    // Get Battle ID by Manager name
    // return null if Manager not binded to any Battle
    public String getBindedBattleIDByManager(String manager) {
        for (Map.Entry<String, String> pair : bindedBattles.entrySet()) {
            if (manager.equals(pair.getValue())) {
                return activeBattles.get(pair.getKey()).ID();
            }
        }

        return null;
    }

    // Check if activeBattles contains ID
    public boolean hasActiveBattlesTheID(String battleID) {
        return activeBattles.containsKey(battleID);
    }

    // Check if bindedBattles contains ID
    public boolean hasBindedBattlesTheID(String battleID) {
        return bindedBattles.containsKey(battleID);
    }

    // Check if Battle with the ID from bindedBattles list contains the manager
    public boolean isManagerBindedToBattle(String battleID, String manager){
        return bindedBattles.get(battleID).contains(manager);
    }

    // Check the manager binded to any battle
    public boolean isManagerBindedToAnyBattle(String manager){
        return bindedBattles.containsValue(manager);
    }

    // Bind the Battle to the Manager
    // Add username to list with Battle ID
    public void bindBattle(String battleID, String manager) throws BattleNotFoundInActiveException, BattleAlreadyBindedException, ManagerAlreadyBindedException {

        // if Battle ID not exists in activeBattles
        if (!hasActiveBattlesTheID(battleID)) {

            throw new BattleNotFoundInActiveException();
        }

        // if Battle already binded to another manager
        if (hasBindedBattlesTheID(battleID)) {

            throw new BattleAlreadyBindedException();
        }

        // if username already binded to any Battle
        if (isManagerBindedToAnyBattle(manager)){
            throw new ManagerAlreadyBindedException();
        }

        bindedBattles.put(battleID, manager);
    }

    // Unbind the Username and the Battle ID
    // Remove username from list with Battle ID
    public void unbindBattle(String battleID, String manager) throws BattleNotFoundInActiveException, ManagerNotBindedException, BattleNotFoundInBindedException {

        // if Battle ID not exists in activeBattles
        if (!hasActiveBattlesTheID(battleID)) {

            throw new BattleNotFoundInActiveException();
        }

        // if Battle not binded (not in bindedBattle map)
        if (!hasBindedBattlesTheID(battleID)) {
            throw new BattleNotFoundInBindedException();
        }

        if (!isManagerBindedToBattle(battleID, manager)) {
            throw new ManagerNotBindedException();
        }

        bindedBattles.remove(battleID);
    }

    // Unbind the Manager from old Battle and bind to Battle with this ID
    // `/btl join`
    public void rebindBattle(String newID, String manager) throws BattleNotFoundInActiveException, BattleAlreadyBindedException, BattleRoundsException {

        try {
            bindBattle(newID, manager);
        } catch (BattleNotFoundInActiveException | BattleAlreadyBindedException e) {
            throw e;

        } catch (ManagerAlreadyBindedException e) {

            String id = getBindedBattleIDByManager(manager);
            if (id != null) {
                try {
                    unbindBattle(id, manager);
                } catch (BattleNotFoundInBindedException | ManagerNotBindedException | BattleNotFoundInActiveException ex) {
                    ex.printStackTrace();

                }
            } else {
                throw new BattleRoundsException("Battle ID not found (null)");
            }

            try {
                bindBattle(newID, manager);
            } catch (ManagerAlreadyBindedException ex) {
                throw new BattleRoundsException("Manager already binded to Battle");
            }
        }


    }

    // Check the username in bindedBattles list, return array of IDs
    // return null if username is not binded with any battle
    @Deprecated
    public ArrayList<String> getBindedBattlesIDs(String manager) {

        ArrayList<String> IDs = new ArrayList<>();

        for (Map.Entry<String, String> pair : bindedBattles.entrySet()) {
            if (manager.equals(pair.getValue())) {
                IDs.add(pair.getKey());
            }
        }

        if (IDs.size() == 0) return null;

        return IDs;
    }

    // Add battle to Battle List
    public void addBattle(Battle battle) {
        activeBattles.put(battle.ID(), battle);
    }

    // Delete Battle from Battle List
    public void deleteBattle(String ID) {
        for (Map.Entry<String, Battle> entry: activeBattles.entrySet()) {
            String btlID = entry.getKey();
            Battle btl = entry.getValue();

            if (ID.equals(btlID)) {
                btl.stop();
                activeBattles.remove(btlID);

                return;
            }
        }
    }

    // Delete Battle from Battle List
    public void deleteBattle(Battle btl) {
        if (activeBattles.containsValue(btl)) {
            btl.stop();
            activeBattles.remove(btl.ID());
        }
    }

    // Show Battle List IDs in CLI
    public void showAllBattlesCLI() {
        for (Map.Entry<String, Battle> entry : activeBattles.entrySet()){
            System.out.println(entry.getKey());
        }
    }

    // Save Battle to file
    public void saveBattle(String battlename) {

    }

    // Load Battle from file
    public void loadBattle(String battlename) {

    }

}
