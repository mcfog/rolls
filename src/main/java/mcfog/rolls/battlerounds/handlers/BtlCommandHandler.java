package mcfog.rolls.battlerounds.handlers;

import mcfog.rolls.battlerounds.Battle;
import mcfog.rolls.battlerounds.BattleManager;
import mcfog.rolls.battlerounds.commands.BtlCommand;
import mcfog.rolls.battlerounds.exceptions.*;
import mcfog.rolls.util.Reference;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;


public class BtlCommandHandler {

    public static void handle(MinecraftServer server, ICommandSender sender, String[] args) {
        Logger logger = LogManager.getLogger(Reference.MODID);

        String response = "Help on under construction";

//        logger.info("-----BTL handler-----");
//        logger.info("isSinglePlayer: " + server.isSinglePlayer());
//        logger.info("Player: " + sender.getName());
//        logger.info("Args: " + Arrays.stream(args).map(s -> "["+s+"]").collect(Collectors.joining()));
//        logger.info("---------------------");

        // args to Lower Case
        args = Arrays.stream(args).map(String::toLowerCase).toArray(String[]::new);

        if (args.length !=0 && args[0] != null) {
            switch (args[0]){
                case BtlCommand.SUBCOMMAND_BTL_CREATE:
                    response = btlCreate(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_JOIN:
                    response = btlJoin(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_STOP:
                    response = btlStop(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_ADD:
                    response = btlAdd(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_LS:
                    response = btlLS(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_CHREACT:
                    response = btlChReact(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_SETREACT:
                    response = btlSetReact(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_MAKEORDER:
                    response = btlMakeOrder(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_NEXTROUND:
                    response = btlNextRound(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_ENDTURN:
                    response = btlEndTurn(args, sender, server);
                    break;

                case BtlCommand.SUBCOMMAND_BTL_LSORDER:
                    response = btlLSOrder(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_CURR:
                    response = btlCurr(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_DEL:
                    response = btlDel(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_SAVE:
                    response = btlSave(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_LOAD:
                    response = btlLoad(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_LSBL:
                    response = btlLSBL(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_LSB:
                    response = btlLSB(args, sender.getName());
                    break;

                case BtlCommand.SUBCOMMAND_BTL_DELBTL:
                    response = btlDELBTL(args, sender.getName());
                    break;

                default:
                        response = "Unknowing command";
            }
        } else{
            //response = "WTF";
        }

        // for custom responses in methods
        if (response.isEmpty()){
            return;
        }

        sendMessage(sender, response);
    }

    protected static String btlCreate(String[] args, String manager) {

        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        String response;

        Battle btl = Battle.create();
        BattleManager.INSTANCE.addBattle(btl);

        try {
            BattleManager.INSTANCE.bindBattle(btl.ID(), manager);

        } catch (BattleNotFoundInActiveException e) {
            response = "Battle not in Battle list. Try again.";

            return response;

        } catch (BattleAlreadyBindedException e) {
            response = "Battle already binded to any Manager";

            return response;

        } catch (ManagerAlreadyBindedException e) {
            response = "Battle was created, but your are already binded to any Battle. Use `/btl join` to join to created Battle";

            return response;
        }

        response = "Battle was created";

        return response;
    }

    protected static String btlJoin(String[] args, String manager){

        if (!checkArgsSize(args, 2)) {
            return "Incorrect command syntax";
        }

        String response;

        String s_battleOrder = args[1];
        int battleOrder;
        try{
            battleOrder = Integer.parseInt(s_battleOrder);
        } catch (NumberFormatException e) {
            return "Enter the sequence number of Battle ID";
        }


        ArrayList<String> ids = new ArrayList<>(BattleManager.INSTANCE.getActiveBattles().keySet());

        if (battleOrder > ids.size() - 1 || battleOrder < 0) {
            return "Enter the sequence number within the boundaries";
        }

        String newID = ids.get(battleOrder);

        try {
            BattleManager.INSTANCE.rebindBattle(newID, manager);

        } catch (BattleNotFoundInActiveException e) {
            response = "Battle not exists in Battle list. Use `/btl create` to create new Battle.";

            return response;

        } catch (BattleAlreadyBindedException e) {
            response = "Battle with id " + newID + " already binded to any Manager";

            return response;

        } catch (BattleRoundsException e) {
            response = "Some error. You're not joined.";

            return response;
        }

        response = "You were joined to Battle id: " + newID;

        return response;
    }

    protected static String btlStop(String[] args, String manager){
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }
        StringBuilder sb = new StringBuilder();
        String response;

        String battleID;

        battleID = BattleManager.INSTANCE.getBindedBattleIDByManager(manager);

        if (battleID != null){

            try {
                BattleManager.INSTANCE.unbindBattle(battleID, manager);
            } catch (BattleNotFoundInActiveException e) {
                response = "Battle not exists in Battle list";
                sb.append(response);
                sb.append("\n");

            } catch (BattleNotFoundInBindedException e) {
                response = "Battle not binded";
                sb.append(response);
                sb.append("\n");

            } catch (ManagerNotBindedException e) {
                return "You are not joined to Battle";
            }
            BattleManager.INSTANCE.deleteBattle(battleID);

        } else {
            return "You are not joined to Battle";
        }

        response = "Battle id: " + battleID + " was stopped";
        sb.append(response);

        return sb.toString();
    }

    protected static String btlAdd(String[] args, String manager) {
        if (args.length < 2) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;
        String user = args[1];
        String[] userlist = Arrays.copyOfRange(args, 1, args.length);

        btl.addUser(userlist);

        if (args.length == 2) {
            response = "User " + user + " has been added";
        } else {
            response = "Users " + Arrays.stream(userlist).map(s -> "[" + s + "]").collect(Collectors.joining(", ")) + " have been added";
        }

        return response;
    }

    protected static String btlLS(String args[], String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;
        String[] userlist = btl.getUserList();

        String users = String.join(", ", userlist);

        if (users.length() != 0){
            response = "Users in Battle: " + users;
        } else {
            response = "No users in Battle";
        }

        return response;
    }

    protected static String btlChReact(String[] args, String manager) {
        if (!checkArgsSize(args, 3)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;

        String s_reaction = args[2];
        double reaction = Double.parseDouble(s_reaction);
        String username = args[1];

        btl.setUserReaction(reaction, username);

        response = "Change reaction to " + s_reaction + " for user " + username;

        return response;
    }

    protected static String btlSetReact(String[] args, String manager) {
        if (args.length < 1) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;
        String[] s_reactions = Arrays.copyOfRange(args, 1, args.length);

        // if reaction length != Battle user list
        if (s_reactions.length != btl.getUserList().length) {
            return "Length of reactions not equal length of Battle user list";
        }

        double[] reactions = Arrays.stream(s_reactions).mapToDouble(Double::parseDouble).toArray();

        btl.setReactionsByUserList(reactions);

        response = "Set reactions " + String.join(", ", s_reactions) + " for users in Battle";

        return response;
    }

    protected static String btlMakeOrder(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;

        btl.makeOrder();

        response = "`/btl makeorder` under construct. Don't use it.";

        return response;
    }

    protected static String btlNextRound(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;
        btl.nextRound();

        response = "Next Round started";

        return response;
    }

    protected static String btlEndTurn(String[] args, ICommandSender sender, MinecraftServer server) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        String manager = sender.getName();

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        if (btl.isCurrentOrderEmpty()) {
            return "No users in order list. Do next round";
        }

        btl.endTurn();

        // Send messages
        sendEndTurnMessages(server, sender, btl);

        // custom response logic
        return "";
    }

    // Send messages about End Turn to Manager and Player
    private static void sendEndTurnMessages(MinecraftServer server, ICommandSender sender, Battle btl) {
        String baseEndTurnMessage = "End Turn";

        // send baseEndTurnMessage to all players in Battle
        sendMessageToBattleUsers(server, btl, baseEndTurnMessage);
        // send baseEndTurnMessage to Manager
        if (!btl.getUserListAsList().contains(sender.getName().toLowerCase())) {
            sendMessage(sender, baseEndTurnMessage);
        }

        // send "Round end message" if round ends
        if (btl.isCurrentOrderEmpty()) {
            String endRoundMessage = "End of the Round";

            sendMessageToBattleUsers(server, btl, endRoundMessage);

            if (!btl.getUserListAsList().contains(sender.getName().toLowerCase())) {
                sendMessage(sender, endRoundMessage);
            }

            return;
        }

        // Send message with curr player

        String message;
        String currname = btl.getCurrentOrderUsername();
        message = "Current Turn player " + currname;

        // Send to manager
        sendMessage(sender, message);

        ICommandSender currplayer = server.getPlayerList().getPlayerByUsername(currname);
        if (currplayer != null) {
            message = "Your Turn";
            sendMessage(currplayer, message);
        }
    }

    // Send message to all Players in the Battle
    private static void sendMessageToBattleUsers(MinecraftServer server, Battle btl, String message) {
        for (String battlePlayer : btl.getUserList()) {
            ICommandSender player = server.getPlayerList().getPlayerByUsername(battlePlayer);
            if (player != null) {
                sendMessage(player, message);
            }
        }
    }

    protected static String btlLSOrder(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        if (btl.isCurrentOrderEmpty()) {
            return "No users in order list";
        }

        String response;
        String currentuser = btl.getCurrentOrderUsername();

        StringBuilder sb = new StringBuilder();

        sb.append("\n");
        sb.append("Order list:");
        sb.append("\n");

        for (Map.Entry<String, Double> entry : btl.getCurrentOrderList().entrySet()) {
            double reaction = entry.getValue();
            String username = entry.getKey();

            sb.append(username);
            sb.append(" ");

            if (username.equals(currentuser)){
                sb.append(reaction + " <-");
            } else {
                sb.append(reaction);
            }


            sb.append("\n");
        }


        // Del last "\n"
        sb.deleteCharAt(sb.length() - 1);
        response = sb.toString();


        return response;
    }

    protected static String btlCurr(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        if (btl.isCurrentOrderEmpty()) {
            return "No users in order list";
        }

        String response;
        String username;

        username = btl.getCurrentOrderUsername();

        response = "Current turn: " + username;

        return response;
    }

    protected static String btlDel(String[] args, String manager) {
        if (args.length < 2) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;

        if (args.length == 2) {

            String username = args[1];

            btl.deleteUser(username);

            response = "User " + username + " has been deleted from Battle";

        } else {

            String[] usernames = Arrays.copyOfRange(args, 1, args.length);

            btl.deleteUser(usernames);

            String users = String.join(", ", usernames);

            response = "Users " + users + " have been deleted from Battle";

        }

        return response;
    }

    protected static String btlSave(String[] args, String manager) {
        if (!checkArgsSize(args, 2)) {
            return "Incorrect command syntax";
        }

        String response;

        String battlename = args[1];

        BattleManager.INSTANCE.saveBattle(battlename);

        response = "`/btl save` under construct. Don't use it.";

        return response;
    }

    protected static String btlLoad(String[] args, String manager) {
        if (!checkArgsSize(args, 2)) {
            return "Incorrect command syntax";
        }
        String response;

        String battlename = args[1];
        BattleManager.INSTANCE.loadBattle(battlename);

        response = "`/btl load` under construct. Don't use it.";

        return response;
    }

    protected static String btlLSBL(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }
        String response;

        SortedMap<String, String> idtoplayer = new TreeMap<>();

        for (SortedMap.Entry<String, Battle> entry : BattleManager.INSTANCE.getActiveBattles().entrySet()) {
            idtoplayer.put(entry.getKey(), "none");
            //idtoplayer.putIfAbsent(entry.getKey(), "none");
        }

        for (Map.Entry<String, String> entry : BattleManager.INSTANCE.getBindedBattles().entrySet()){
            idtoplayer.replace(entry.getKey(), entry.getValue());
        }


        StringBuilder sb = new StringBuilder();

        sb.append("Battles list:");
        sb.append("\n");

        int index = 0;
        for (Map.Entry<String, String> entry : idtoplayer.entrySet()){
            sb.append(index);
            sb.append(" : ");
            sb.append(entry.getKey());
            sb.append(" : ");
            sb.append(entry.getValue());

            if (manager.equals(entry.getValue())){
                sb.append(" <-");
            }

            sb.append("\n");
            index++;
        }

        if (sb.length() != 0) {
            // Del last "\n"
            //sb.deleteCharAt(sb.length() - 1);
            response = sb.toString();
        } else {
            return "No Battles";
        }

        return response;
    }

    protected static String btlLSB(String[] args, String manager) {
        if (!checkArgsSize(args, 1)) {
            return "Incorrect command syntax";
        }

        final Battle btl = BattleManager.INSTANCE.getBindedBattleByManager(manager);

        if (btl == null) {
            return "You are not joined to any Battle.";
        }

        String response;

        String id = btl.ID();

        ArrayList<String> keys = new ArrayList<>(BattleManager.INSTANCE.getActiveBattles().keySet());
        int index = keys.indexOf(id);

        response = index + " : " + id;

        return response;
    }


    // Deleting range of unbinded Battles
    private static String btlDELBTL(String[] args, String manager) {
        if (args.length < 2) {
            return "Incorrect command syntax";
        }

        String response;

        String[] s_battleNumbers = Arrays.copyOfRange(args, 1, args.length);
        ArrayList<String> ids = new ArrayList<>(BattleManager.INSTANCE.getActiveBattles().keySet());

        if (s_battleNumbers.length > ids.size()) {
            return "The amount of numbers is greater than the amount of Active Battles.";
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder sb_errors = new StringBuilder();

        for (String s_battleNumber : s_battleNumbers){
            int battleOrder;

            // Cast to Int
            try {
                battleOrder = Integer.parseInt(s_battleNumber);
            } catch (NumberFormatException e) {
                sb_errors.append("The " + s_battleNumber + " is not a number");
                sb_errors.append("\n");
                continue;
            }

            // OutOfRange check
            if (battleOrder < 0 || battleOrder > ids.size() - 1){
                sb_errors.append("The number " + s_battleNumber + " out of Active Battles range");
                sb_errors.append("\n");
                continue;
            }

            // If the Battle is binded
            if (BattleManager.INSTANCE.hasBindedBattlesTheID(ids.get(battleOrder))) {
                sb_errors.append("Battle with number " + s_battleNumber + " is binded");
                sb_errors.append("\n");
                continue;
            }

            BattleManager.INSTANCE.deleteBattle(ids.get(battleOrder));
            sb.append("Battle number " + s_battleNumber + " has been removed");
            sb.append("\n");
        }

        response = "Battle Deleting:" + "\n" + sb_errors.toString() + "\n" + sb.toString();

        return response;
    }


    private static boolean checkArgsSize(String[] args, int want) {
        if (args.length != want) {
            return false;
        }
        return true;
    }

    public static void sendMessage(ICommandSender sender, String message){
        ITextComponent iTextComponent = new TextComponentString(message);
        sender.sendMessage(iTextComponent);
    }
}
