package mcfog.rolls.proxy;

import mcfog.rolls.battlerounds.commands.BtlCommand;
import mcfog.rolls.commands.CommandRegister;
import mcfog.rolls.handlers.ChatEventHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

public class CommonProxy {
    public void preInit(FMLPreInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(new ChatEventHandler());
    }

    public void init(FMLInitializationEvent event)
    {

    }

    public void postInit(FMLPostInitializationEvent event) {

    }

    public void serverStarting(FMLServerStartingEvent event){
        //CommandRegister.INSTANCE.addCommand(new TestBtlCommand());
        CommandRegister.INSTANCE.addCommand(new BtlCommand());
        CommandRegister.INSTANCE.Register(event);
    }

}