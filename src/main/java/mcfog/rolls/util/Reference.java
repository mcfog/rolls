package mcfog.rolls.util;

public class Reference {
    public static final String MODID = "rolls";
    public static final String NAME = "Rolls";
    public static final String VERSION = "1.0";
    public static final String CLIENT = "mcfog.rolls.proxy.ClientProxy";
    public static final String COMMON = "mcfog.rolls.proxy.CommonProxy";
}
