package mcfog.rolls.util;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Arrays;

public class Permissions {

    public static boolean isOp(String username){
        boolean result;

        String[] oplist = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOppedPlayerNames();

        result = Arrays.asList(oplist).contains(username);

        return result;
    }

    public static boolean isSinglePlayer() {

        boolean result;

        result = Side.CLIENT == FMLCommonHandler.instance().getSide();

        return result;
    }
}
