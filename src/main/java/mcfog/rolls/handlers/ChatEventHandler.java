package mcfog.rolls.handlers;

import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


public class ChatEventHandler {

    @SubscribeEvent
    public void onServerChatEvent(ServerChatEvent event) {

        System.out.println("onServerChatEvent");

        handleServerRollCommand(event);
    }

    private void handleServerRollCommand(ServerChatEvent event) {

        String message = event.getMessage();
        String username = event.getUsername();
        String result = "";

        if (RollsHandler.hasRollCommand(message)) {
            System.out.println(username + ": " + message);

            // cancel the vanilla chat
            event.setCanceled(true);

            result = RollsHandler.handle(message, username);

            RollsHandler.sendMessageToWorld(result);
        }
    }
}
